﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace Albelli.API.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            context.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            context.Response.ReasonPhrase = "Something weird happened!";
        }
    }
}