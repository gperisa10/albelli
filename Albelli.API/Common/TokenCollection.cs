﻿using System;
using System.Collections.Generic;

namespace Albelli.API.Common
{
    public sealed class TokenCollection
    {
        public Dictionary<string, string> ActiveTokens = new Dictionary<string, string>();

        private static readonly Lazy<TokenCollection> lazy =
            new Lazy<TokenCollection>(() => new TokenCollection());

        public static TokenCollection Instance { get { return lazy.Value; } }

        private TokenCollection()
        {
        }
    }
}