﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Albelli.Model;
using AutoMapper;
using Albelli.Model.ViewModels;
//using Albelli.Model.Utilities;
using System.Net.Http;
using System.Net;
using Albelli.Model.Models;
using System;
using Albelli.DAL;
using Albelli.Model.Validation;

namespace Albelli.API.Controllers
{
    public class CustomersController : ApiController
    {
        private IRepository<Customer> repository = null;
        public CustomersController()
        {
            repository = new Repository<Customer>();
        }

        public CustomersController(IRepository<Customer> repository)
        {
            this.repository = repository;
        }

        // GET: V1/Customers
        [Authorize]
        public IHttpActionResult GetCustomers()
        {
            var result = repository.GetAll().ToList();

            Mapper.Initialize(cfg => cfg.CreateMap<Customer, CustomerViewModel>());
            var viewModelList = Mapper.Map<List<Customer>, List<CustomerViewModel>>(result);

            return Ok(viewModelList);
        }

        // GET: V1/Customers/5
        public IHttpActionResult GetCustomers(int id)
        {
            var result = repository.GetByID(id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        // POST: V1/Customers
        [HttpPost]
        public IHttpActionResult AddCustomer([FromUri]string name, [FromUri]string email)
        {
            var customer = new Customer() { Name = name, Email = email };
            var validation = customer.ValidateCustomer();

            if (validation.IsValid == false)
            {
                return Content(HttpStatusCode.BadRequest, validation.Errors.Select(x => x.ErrorMessage).ToList());
            }

            var result = repository.Insert(new Customer() { Name = name, Email = email });

            Mapper.Initialize(cfg => cfg.CreateMap<Customer, CustomerViewModel>());
            var viewModelList = Mapper.Map<Customer, CustomerViewModel>(result);

            return Ok(viewModelList);
        }

    }
}