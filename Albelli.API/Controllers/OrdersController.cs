﻿using Albelli.DAL;
using Albelli.Model;
using Albelli.Model.Models;
using Albelli.Model.Validation;
using System;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace Albelli.API.Controllers
{
    public class OrdersController : ApiController
    {
        private IRepository<Order> repository = null;
        public OrdersController()
        {
            repository = new Repository<Order>();
        }

        public OrdersController(IRepository<Order> repository)
        {
            this.repository = repository;
        }

        // POST: V1/Customers/[id]/Orders
        [HttpPost]
        public IHttpActionResult Customers([FromUri]string price, int id)
        {
            decimal decParsed = 0;
            decimal.TryParse(price, out decParsed);

            var order = new Order() { CustomerID = id, Price = decParsed, DateCreated = DateTime.Now };

            var validation = order.ValidateOrder();
            if (validation.IsValid == false)
            {
                return Content(HttpStatusCode.BadRequest, validation.Errors.Select(x => x.ErrorMessage).ToList());
            }

            var result = repository.Insert(order);
            return Ok();
        }
    }
}
