﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Albelli.API.Logins
{
    public interface ILogin
    {
        int CheckCredentials(string username, string password);

        string GetClientIp(string x_forwarded_for, string userhostaddress);

        bool CheckIsLoggedIn(string userName);

        bool LogInUser(string userName, string ip, string browser);

        bool LogoutUser(string userName);
    }
}