﻿using Microsoft.Owin.Hosting;
using System;

namespace Albelli.API
{
    public class Program
    {
        static void Main(string[] args)
        {
            string baseAddress = "http://localhost:9000/";

            // Start OWIN host 
            using (WebApp.Start<StartUp>(url: baseAddress))
            {

                Console.WriteLine("Server is started...http://localhost:9000/");
                Console.WriteLine("Press any key to exit.");
                Console.ReadLine();
            }
        }
    }
}