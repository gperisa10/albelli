﻿using Albelli.Model.Models;
using FluentValidation.Results;

namespace Albelli.Model.Validation
{
    public static class CustomerValidation
    {
        public static ValidationResult ValidateCustomer(this Customer customer)
        {
            CustomerValidator validator = new CustomerValidator();
            ValidationResult results = validator.Validate(customer);
            return results;
        }
    }
}
