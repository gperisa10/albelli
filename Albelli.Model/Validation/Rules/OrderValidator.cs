﻿using FluentValidation;
using Albelli.Model.Models;

namespace Albelli.Model.Validation
{
    public class OrderValidator : AbstractValidator<Order>
    {
        public OrderValidator()
        {
            RuleFor(order => order.CustomerID).NotNull().GreaterThan(0);
            RuleFor(order => order.Price).NotNull();
        }
    }
}
