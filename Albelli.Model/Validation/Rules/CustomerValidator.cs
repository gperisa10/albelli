﻿using FluentValidation;
using Albelli.Model.Models;

namespace Albelli.Model.Validation
{

    public class CustomerValidator : AbstractValidator<Customer>
    {
        public CustomerValidator()
        {
            RuleFor(customer => customer.Name).NotEmpty().MaximumLength(100);
            RuleFor(customer => customer.Email).NotEmpty().EmailAddress().MaximumLength(100);
        }
    }
}
