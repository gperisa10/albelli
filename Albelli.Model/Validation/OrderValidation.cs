﻿using Albelli.Model.Models;
using FluentValidation.Results;

namespace Albelli.Model.Validation
{
    public static class OrderValidation
    {
        public static ValidationResult ValidateOrder(this Order order)
        {
            OrderValidator validator = new OrderValidator();
            ValidationResult results = validator.Validate(order);
            return results;
        }
    }
}
