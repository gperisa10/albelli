﻿namespace Albelli.Model.ViewModels
{
    public class CustomerViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}