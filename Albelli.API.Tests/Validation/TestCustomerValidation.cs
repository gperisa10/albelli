﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Albelli.Model.Models;
using System.Linq;
using FluentValidation.Results;
using Albelli.Model.Validation;

namespace Albelli.Tests.API
{
    [TestClass]
    public class TestCustomersValidation
    {
        [TestMethod]
        public void TestValidateCustomer_1()
        {
            var customerValidation = CustomerValidation.ValidateCustomer(new Customer { Name = "Albelli", Email = "aa@aa.com" });
            Assert.IsInstanceOfType(customerValidation, typeof(ValidationResult));
            Assert.IsNotNull(customerValidation);
            Assert.AreEqual(0, customerValidation.Errors.Count());
        }

        [TestMethod]
        public void TestValidateOrder_2()
        {
            var customerValidation = CustomerValidation.ValidateCustomer(new Customer { Name = "Albelli", Email = "aa.com" });
            Assert.IsInstanceOfType(customerValidation, typeof(ValidationResult));
            Assert.AreNotEqual(0, customerValidation.Errors.Count());
        }
    }
}
