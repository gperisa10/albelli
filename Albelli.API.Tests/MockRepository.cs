﻿using System;
using System.Collections.Generic;
using System.Linq;
using Albelli.DAL;

namespace Albelli.Tests
{
    class MockRepository<T> : IRepository<T> where T : class
    {
        private List<T> data = new List<T>();

        public IEnumerable<T> GetAll()
        {
            return data;
        }

        public T GetByID(object id)
        {
            T obj = (T)Activator.CreateInstance(typeof(T));
            return obj;
        }

        public T Insert(T obj)
        {
            data.Add(obj);
            Save();
            return obj;
        }

        public T Update(T obj)
        {
            T existing = data.FirstOrDefault();
            existing = obj;
            return obj;
        }

        public void Delete(object id)
        {
            data.RemoveAt(0);
        }

        public void Save()
        {
            //nothing here
        }

        public static T GetObject<T>() where T : new()
        {
            return new T();
        }
    }
}
