﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Albelli.Model;
using System.Collections.Generic;
using Albelli.Model.Models;
using System.Linq;

namespace Albelli.Tests.DAL
{
    [TestClass]
    public class TestOrders
    {
        [TestMethod]
        public void TestGetAllOrders()
        {
            var repository = new Repository<Order>();
            var response = repository.GetAll().ToList();
            Assert.IsInstanceOfType(response, typeof(List<Order>));
            Assert.IsNotNull(response);
        }
    }
}
