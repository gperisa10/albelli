﻿using Albelli.Model;
using Albelli.Model.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Albelli.API.Tests.DAL
{
    [TestClass]
    public class TestCustomers
    {
        [TestMethod]
        public void TestGetAllCustomers()
        {
            var repository = new Repository<Customer>();
            var response = repository.GetAll().ToList();
            Assert.IsInstanceOfType(response, typeof(List<Customer>));
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void TestGetCustomerByID()
        {
            var repository = new Repository<Customer>();
            var response = repository.GetByID(1);
            Assert.IsInstanceOfType(response, typeof(Customer));
            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Orders);
        }


    }
}
