﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Albelli.API.Controllers;
using System.Web.Http;
using Albelli.Model.Models;
using System.Web.Http.Results;
using Albelli.Model.ViewModels;

namespace Albelli.Tests.API
{
    [TestClass]
    public class TestCustomers
    {
        [TestMethod]
        public void TestCustomersCtrlGetAll()
        {
            var repo = new MockRepository<Customer>();
            var controller = new CustomersController(repo);
            var response = controller.GetCustomers();
            Assert.IsInstanceOfType(response, typeof(IHttpActionResult));
            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void TestCustomerCtrlGetByID()
        {
            var repo = new MockRepository<Customer>();
            var controller = new CustomersController(repo);
            var response = controller.GetCustomers(1);
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<Customer>), "Response is not OK");
        }

        [TestMethod]
        public void TestCustomerCtrlAddNew()
        {
            var repo = new MockRepository<Customer>();
            var controller = new CustomersController(repo);
            var response = controller.AddCustomer("testCustomer", "test@test.com");
            Assert.IsNotNull(response);
            Assert.IsInstanceOfType(response, typeof(OkNegotiatedContentResult<CustomerViewModel>), "Response is not OK");
        }
    }
}