﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Albelli.API.Controllers;
using Albelli.Model;
using System.Net.Http;
using System.Collections.Generic;
using Albelli.Model.Models;
using System.Web.Http;
using System.Linq;

namespace Albelli.Tests.API
{
    [TestClass]
    public class TestOrders
    {
        [TestMethod]
        public void TestAddOrder()
        {
            var repo = new MockRepository<Order>();
            var controller = new OrdersController(repo);
            var response = controller.Customers("11.11", 1);
            Assert.IsInstanceOfType(response, typeof(IHttpActionResult));
            Assert.IsNotNull(response);
        }
    }
}
